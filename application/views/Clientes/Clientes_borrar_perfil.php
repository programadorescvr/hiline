<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 24/11/17
 * Time: 03:00 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li><a href="<?= site_url('Clientes/Borrar_cliente/'.$cliente['id_cliente']) ?>">Editar cliente</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Borrar cliente</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php if (!empty(validation_errors())) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h5><i class="icon fa fa-warning"></i>Alerta</h5>
                                    <h6><?= validation_errors()?></h6>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="row">
                                <form action="<?= site_url('Clientes/Borrar_cliente/'.$cliente['id_cliente']) ?>" method="post" class="form" id="form-borrar-cliente">
                                    <h4>Cliente <?= $cliente['nombre_cliente']?> Codigó <?= $cliente['id_cliente']?></h4>
                                    <div class="col-md-8 form-group">
                                        <label for="borrar">Escriba 'BORRAR' si desea borrar el reginstro</label>
                                        <input type="text" id="borrar" name="borrar" class="form-control" required/>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <br>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Borrar</button>
                                        <a href="<?=site_url()?>" class="btn btn-default"><i class="fa fa-times"></i> Cancelar</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
