<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 12:17 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Clientes</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Ver Clientes</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    if (isset($mensaje)){
                    ?>
                        <div class="row"><?= $mensaje?></div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="pull-right"><a href="<?= site_url('Clientes/Crear_cliente')?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Crear Cliente</a></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered wy-table-backed datatable"  id="tabla-clientes">
                                <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <!--<th>
                                            Enfoque
                                        </th>-->

                                        <!--<th>
                                            Ultima Llamada
                                        </th>-->
                                        <th>
                                            Bolsa de llamadas
                                        </th>
                                        <th>
                                            # Meses
                                        </th>
                                        <th>
                                            Horario
                                        </th>
                                        <!--<th>
                                            Tutor
                                        </th>-->
                                        <th>
                                            Acción
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (isset($clientes) && !empty($clientes) ){
                                    foreach ($clientes as $item){
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $item['id_cliente']?>
                                            </td>
                                            <td>
                                                <?= $item['nombre_cliente']?>
                                            </td>
                                            <!--<td>
                                                <?= $item['enfoque']?>
                                            </td>-->
                                            <!--<td>
                                                <?= $item['fecha']?>
                                            </td>-->
                                            <td>
                                                <?= $item['n_llamadas']?>
                                            </td>
                                            <td>
                                                <?= ceil($item['llamadas']/21)?>
                                            </td>
                                            <td>
                                                <?= $item['horario_llamada']?>
                                            </td>
                                            <!--<td>
                                                <?= $item['nombre_tutor']?>
                                            </td>-->
                                            <td>
                                                <a href="<?= site_url('Clientes/Ver_cliente/'.$item['id_cliente'])?>" class="btn btn-success"><i class="fa fa-eye" aria-hidden="true"></i> Ver</a>
                                                <a href="<?= site_url('Clientes/Editar_cliente/'.$item['id_cliente'])?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                                                <a href="<?= site_url('Clientes/Borrar_cliente/'.$item['id_cliente'])?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Borrar</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="box-footer">
                <div class="pull-right"><a href="<?= site_url('Clientes/Crear_cliente')?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Crear Cliente</a></div>
            </div>
        </div>
    </section>
</div>

