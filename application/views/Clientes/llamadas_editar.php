<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 27/11/17
 * Time: 11:43 AM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li><a href="<?=site_url('Clientes/Ver_cliente/'.$llamada['id_cliente'])?>">Ver cliente</a></li>
            <li><a href="<?=site_url('Clientes/Editar_llamada/'.$llamada['id_reporte'])?>">Crear llamada</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Editar llamada</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <form action="<?= site_url('Clientes/Editar_llamada/'.$llamada['id_reporte']) ?>" method="post" class="form" id="form-editar-llamada">
                <div class="box-body">
                    <div class="container-fluid">
                        <?php if (!empty(validation_errors())) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h5><i class="icon fa fa-warning"></i>Alerta</h5>
                                        <h6><?= validation_errors()?></h6>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <h4><?= $llamada['nombre_cliente'] ?></h4>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="tutor">Tutor</label>
                                        <select name="tutor" id="tutor" class="form-control" required>
                                            <option></option>
                                            <?php
                                            if (isset($tutores) && !empty($tutores)){
                                                foreach ($tutores as $item) {
                                                    ?>
                                                    <option value="<?=$item['id_tutor']?>" <?= set_select('tutor',$item['id_tutor'],$item['id_tutor']==$llamada['id_tutor'])?>><?=$item['nombre_tutor']?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="duracion">Duración</label>
                                        <input type="number" id="duracion" name="duracion" class="form-control" value="<?= set_value('duracion',$llamada['duracion'])?>" required/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="fecha_llamada">Fecha llamada</label>
                                        <input type="text" id="fecha_llamada" name="fecha_llamada" class="form-control fecha" required value="<?= set_value('fecha_llamada',$llamada['fecha'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div class="bootstrap-timepicker timepicker">
                                            <label for="hora_llamada">Hora llamada</label>
                                            <input type="text" id="hora_llamada" name="hora_llamada" class="form-control hora" required value="<?= set_value('hora_llamada',$llamada['hora'])?>"/>
                                        </div>
                                    </div>


                                    <div class="col-md-6 form-group">
                                        <label for="tematica">Tematica</label>
                                        <select name="tematica" id="tematica" class="form-control" required="required">
                                            <option></option>
                                            <?php
                                            foreach ($tematicas as $value) {
                                                echo "<option value='".$value['id_tematica']."' ".set_select('tematica',$value['id_tematica'],$value['id_tematica'] == $llamada['id_tematica'])." >".$value['nombre_tematica']."</option>";
                                            }
                                            ?>
                                            <option value="otros">Otros</option>
                                        </select>
                                        <!--<textarea id="tematica" name="tematica" class="form-control" maxlength="149" required><?= set_value('tematica')?></textarea>-->

                                    </div>
                                    <div class="col-md-6 form-group" >
                                        <fieldset disabled id="otros">
                                            <label for="otros">Otros</label>
                                            <input type="text" id="otros" name="otros" class="form-control" value="<?= set_value('otros')?>" required/>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <div>
                                            <label for="fortalezas">Fortalezas</label>
                                            <select name="fortalezas[]" id="fortalezas" multiple="multiple" class="form-control multiple-select">
                                                <?php
                                                foreach ($categorias as $categoria){
                                                    echo '<optgroup label="'.$categoria['nombre_categorias'].'">';
                                                    foreach ($fortalezas as $fortaleza){
                                                        if ($categoria['id_categorias'] == $fortaleza['fk_categorias']) {
                                                            $estado = false;
                                                            foreach ($llamada['fortalezas'] as $item){
                                                                if ($item['fk_fortalezas'] == $fortaleza['id_fortalezas']) {
                                                                    $estado = true;
                                                                    break;
                                                                }
                                                            }
                                                            ?>
                                                            <option value="<?= $fortaleza['id_fortalezas'] ?>" <?= set_select('fortalezas', $fortaleza['id_fortalezas'],$estado) ?>><?= $fortaleza['nombre_fortalezas'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    echo '</optgroup>';
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="recomendaciones">Recomendaciones</label>
                                        <textarea id="recomendaciones" name="recomendaciones" class="form-control" maxlength="149" required><?= set_value('recomendaciones',$llamada['recomendaciones'])?></textarea>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10 ">
                            <a href="<?= site_url('Clientes/Ver_cliente/'.$llamada['id_cliente'])?>" class="btn btn-default pull-left"><i class="fa fa-times"></i> Cancelar</a>
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-pencil"></i> Editar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
