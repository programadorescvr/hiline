<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 22/11/17
 * Time: 03:02 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clientes
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li><a href="<?=site_url('Clientes/Ver_cliente/'.$id_cliente)?>">Ver cliente</a></li>
        </ol>
    </section>

    <section class="content">


        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Ver perfil cliente</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?php
                    if (isset($mensaje)){
                        ?>
                        <div class="row"><?= $mensaje?></div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Datos del cliente</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="ver-perfil">
                                    <thead>
                                    <tr>
                                        <th>Datos</th>
                                        <th>Información</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($cliente) && !empty($cliente)){
                                        ?>
                                        <tr>
                                            <td>Nombre</td>
                                            <td><?= $cliente['nombre_cliente']?></td>
                                        </tr>
                                        <tr>
                                            <td>Celular</td>
                                            <td><?= $cliente['celular']?></td>
                                        </tr>
                                        <tr>
                                            <td>Enfoque</td>
                                            <td><?= $cliente['enfoque']?></td>
                                        </tr>
                                        <tr>
                                            <td>Tutor</td>
                                            <td><?= $cliente['nombre_tutor']?></td>
                                        </tr>
                                        <tr>
                                            <td>Horario Llamada</td>
                                            <td><?= $cliente['horario_llamada']?></td>
                                        </tr>
                                        <tr>
                                            <td>Correo</td>
                                            <td><?= $cliente['correo']?></td>
                                        </tr>
                                        <tr>
                                            <td>Fecha inicio</td>
                                            <td><?= $cliente['fecha_inicio']?></td>
                                        </tr>
                                        <tr>
                                            <td>Bolsa de llamadas</td>
                                            <td><?= $cliente['n_llamadas']?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <h3>Fortalezas</h3>
                            <?php
                                foreach ($categorias as $categoria) {
                                    ?>
                                    <div class="col-md-4">
                                        <h3><?= $categoria['nombre_categorias']?></h3>
                                        <ul class="list-unstyled list-hiline">
                                            <?php
                                            foreach ($fortalezas as $fortaleza){
                                                $activo = 'default';
                                                if ($categoria['id_categorias'] == $fortaleza['fk_categorias']) {
                                                    foreach ($fortalezas_cliente as $item){
                                                        if ($item['fk_fortalezas'] == $fortaleza['id_fortalezas']){
                                                            $activo = 'info';
                                                            break;
                                                        }
                                                    }
                                                    echo '<li><h4><span class="label label-'.$activo.'">' . $fortaleza['nombre_fortalezas'] . '</span></h4></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <h3>Seguimiento</h3>
                            <div class="col-md-6">
                                <div class="info-box bg-aqua-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Llamadas realizadas</span>
                                        <span class="info-box-number"><?=$seguimineto['n_llamdas']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-green-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Duración promedio</span>
                                        <span class="info-box-number"><?=$seguimineto['avg_llamada']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-yellow-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Duración máxima</span>
                                        <span class="info-box-number"><?=$seguimineto['max_llamada']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-purple-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Duración minima</span>
                                        <span class="info-box-number"><?=$seguimineto['min_llamada']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h3>Grafica de tematicas vista</h3>
                                <div class="graficas">
                                    <canvas id="tematicas" height="100%" width="100%"></canvas>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Registro Llamadas</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="pull-right"><a href="<?= site_url('Clientes/Crear_llamada/'.$id_cliente) ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Crear Llamada</a></div>

                <div >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered datatable" id="ver-llamadas">
                                    <thead>
                                    <tr>
                                        <th class="sort-date">Fecha llamada</th>
                                        <th>Tutor</th>
                                        <th>Temática</th>
                                        <th>Duración</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($llamadas) && !empty($llamadas)) {
                                        foreach ($llamadas as $item){
                                            ?>
                                            <tr>
                                                <td><?= $item['fecha_llamada'] ?></td>
                                                <td><?= $item['nombre_tutor'] ?></td>
                                                <td><?= $item['nombre_tematica'] ?></td>
                                                <td><?= $item['duracion'] ?></td>
                                                <td>
                                                    <a href="<?= site_url('Clientes/Ver_llamada/'.$item['id_reporte'])?>" class="btn btn-success"><i class="fa fa-eye" aria-hidden="true"></i> Ver</a>
                                                    <a href="<?= site_url('Clientes/Editar_llamada/'.$item['id_reporte'])?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                                                    <a href="<?= site_url('Clientes/Borrar_llamada/'.$item['id_reporte'])?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Borrar</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right"><a href="<?= site_url('Clientes/Crear_llamada/'.$id_cliente) ?>" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Crear Llamada</a></div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    var tematicas = document.getElementById("tematicas");
    var tematicas_chart = new Chart(tematicas,{
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php foreach ($tematicas as $item) echo $item['n_tematicas'].', '?>
                ],backgroundColor: [
                    <?php foreach ($tematicas as $item) echo 'getRandomColor(), '?>
                ],
                label: 'Temáticas'
            }],
            labels: [
                <?php foreach ($tematicas as $item) echo '\''.$item['nombre_tematica'].'\', '?>
            ]
        },
        options: {
            responsive: true
        }
    });
</script>