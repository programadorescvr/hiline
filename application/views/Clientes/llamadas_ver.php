<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 27/11/17
 * Time: 12:46 PM
 */defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Clientes</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li><a href="<?=site_url('Clientes/Ver_cliente/'.$llamada['id_cliente'])?>">Ver cliente</a></li>
            <li><a href="<?= site_url('Clientes/Ver_llamada/'.$llamada['id_reporte']) ?>">Ver llamada</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Ver llamada</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Datos de la llamada</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="ver-perfil">
                                    <thead>
                                    <tr>
                                        <th>Datos</th>
                                        <th>Información</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (isset($llamada) && !empty($llamada)){
                                        ?>
                                        <tr>
                                            <td>Nombre</td>
                                            <td><?= $llamada['nombre_cliente']?></td>
                                        </tr>
                                        <tr>
                                            <td>Fecha llamada</td>
                                            <td><?= $llamada['fecha_llamada']?></td>
                                        </tr>
                                        <tr>
                                            <td>Tutor</td>
                                            <td><?= $llamada['nombre_tutor']?></td>
                                        </tr>
                                        <tr>
                                            <td>Tematica</td>
                                            <td><?= $llamada['tematica']?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Recomendaciones</h4>
                            <p>
                                <?= $llamada['recomendaciones']?>
                            </p>
                            <div class="row">
                                <?php
                                foreach ($categorias as $categoria) {
                                    ?>
                                    <div class="col-md-4">
                                        <label><?= $categoria['nombre_categorias'] ?></label>
                                        <ul>
                                            <?php
                                            foreach ($llamada['fortalezas'] as $fortaleza) {
                                                if ($categoria['id_categorias'] == $fortaleza['fk_categorias']) {
                                                    ?>
                                                    <li><?= $fortaleza['nombre_fortalezas'] ?></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="<?= site_url('Clientes/Ver_cliente/'.$llamada['id_cliente'])?>" class="btn btn-primary pull-right"><i class="fa fa-arrow-left"></i> Regresar</a>
            </div>
        </div>

    </section>
</div>
