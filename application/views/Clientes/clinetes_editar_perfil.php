<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 23/11/17
 * Time: 11:25 AM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Clientes
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-dashboard"></i> Clientes</a></li>
            <li><a href="<?= site_url('Clientes/Editar_cliente/'.$cliente['id_cliente']) ?>">Editar cliente</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Editar cliente</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <form action="<?= site_url('Clientes/Editar_cliente/'.$cliente['id_cliente']) ?>" method="post" class="form" id="form-editar-cliente">
                <div class="box-body">
                    <div class="container-fluid">
                        <?php if (!empty(validation_errors())) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h5><i class="icon fa fa-warning"></i>Alerta</h5>
                                        <h6><?= validation_errors()?></h6>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <!--<h4>campos obligatorios* </h4>-->
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="nombres">Nombres</label>
                                        <input type="text" id="nombres" name="nombres" class="form-control" value="<?= set_value('nombres',$cliente['nombres'])?>"  required/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="apellidos">Apellidos</label>
                                        <input type="text" id="apellidos" name="apellidos" class="form-control" value="<?= set_value('apellidos',$cliente['apellidos'])?>" required/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="celular">Celular</label>
                                        <input type="number" id="celular" name="celular" class="form-control" required value="<?= set_value('celular',$cliente['celular'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="correo">Correo</label>
                                        <input type="email" id="correo" name="correo" class="form-control" required value="<?= set_value('correo',$cliente['correo'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="horario">Horario</label>
                                        <input type="text" id="horario" name="horario" class="form-control" required value="<?= set_value('horario',$cliente['horario_llamada'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="fecha_inicio">Fecha inicio</label>
                                        <input type="text" id="fecha_inicio" name="fecha_inicio" class="form-control fecha" required value="<?= set_value('fecha_inicio',$cliente['fecha_inicio'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="n_llamadas">Bolsa de llamadas</label>
                                        <input type="number" id="n_llamadas" name="n_llamadas" class="form-control" required value="<?= set_value('n_llamadas',$cliente['n_llamadas'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="tutor">Tutor</label>
                                        <select name="tutor" id="tutor" class="form-control" required>
                                            <option></option>
                                            <?php
                                            if (isset($tutores) && !empty($tutores)){
                                                foreach ($tutores as $item) {
                                                    ?>
                                                    <option value="<?=$item['id_tutor']?>" <?= set_select('tutor',$item['id_tutor'],$item['id_tutor']==$cliente['id_tutor'])?>><?=$item['nombre_tutor']?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="enfoque">Enfoque</label>
                                        <textarea id="enfoque" name="enfoque" class="form-control" maxlength="149" required><?= set_value('enfoque',$cliente['enfoque'])?></textarea>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-pencil"></i> Editar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
