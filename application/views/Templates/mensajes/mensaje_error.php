<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 17/07/2017
 * Time: 5:45 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="alert alert-danger alert-dismissible" id="mensaje">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="fa fa-times" aria-hidden="true"></i> Denegado!</h4>
    <?= $mensaje?>
</div>
