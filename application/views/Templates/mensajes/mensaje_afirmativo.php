<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 17/07/2017
 * Time: 5:48 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="alert alert-info alert-dismissible" id="mensaje">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="fa fa-check"  aria-hidden="true"></i> Hecho!</h4>
    <?= $mensaje?>
</div>
