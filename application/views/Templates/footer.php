<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 20/11/17
 * Time: 03:31 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Main Footer -->
<footer class="main-footer">

    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="http://www.ideco.com.co">www.ideco.com.co</a></strong>
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>public/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url()?>public/dist/js/adminlte.min.js"></script>
<!-- moment -->
<script src="<?= base_url()?>public/plugins/moment/min/moment.min.js"></script>
<!-- datepicker -->
<script src="<?= base_url()?>public/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- DateTable -->
<script src="<?= base_url()?>public/plugins/DataTables/datatables.min.js"></script>
<!--<script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script>-->
<script src="<?= base_url()?>public/plugins/DataTables/moment.datatable.js"></script>
<!-- Select2 -->
<script src="<?= base_url()?>public/plugins/select2/dist/js/select2.min.js"></script>
<!-- timepicker -->
<script src="<?= base_url()?>public/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<!-- Pace-master -->
<script src="<?= base_url()?>public/plugins/pace-master/pace.min.js"></script>
<!-- carousel-2 -->
<script src="<?= base_url()?>public/plugins/carousel-2/dist/owl.carousel.min.js"></script>


<!-- Script hi-line -->
<script src="<?= base_url()?>public/js/script.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>

