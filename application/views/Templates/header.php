<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 20/11/17
 * Time: 03:26 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$nombre_usuario = $this->session->userdata('nombre').' '.$this->session->userdata('apellido');
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hi Line | <?= $titulo?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url()?>public/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter page. However, you can choose any other skin. Make sure you apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="<?= base_url()?>public/dist/css/skins/skin-hi-line.css">
    <!-- Style Hi Line -->
    <link rel="stylesheet" href="<?= base_url()?>public/css/style-hi-line.css">

    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>public/img/icon.png" class="img-bordered img-responsive img-circle">

    <!-- datepicker -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- DateTable -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/DataTables/datatables.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/select2/dist/css/select2.min.css">
    <!-- timepicker -->
    <link rel="stylesheet" href="<?= base_url()?>public/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <!-- Pace-master -->
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/pace-master/templates/load.css')?>">
    <!-- carousel-2 -->
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/carousel-2/dist/assets/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/carousel-2/dist/assets/owl.theme.default.min.css')?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 3 -->
    <script src="<?= base_url()?>public/plugins/jquery/dist/jquery.min.js"></script>
    <!-- chart.js -->
    <script src="<?= base_url()?>public/plugins/chart.js/dist/Chart.js"></script>
    <!-- Google Font
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>
<body class="hold-transition skin-hi-line sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="<?= site_url() ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?= base_url()?>public/img/icon.png" class="img-circle img-responsive" ></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?= base_url()?>public/img/Logo-Hi-line-02.png" class="img-rounded" style="width: auto; height: 50px"></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                           <img src="<?= base_url()?>public/img/icon.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?= $nombre_usuario?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?= base_url()?>public/img/icon.png" class="img-circle" alt="User Image">

                                <p>
                                    <?= $nombre_usuario?>
                                    <small>Tutor</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= site_url('Perfil/Editar_perfil')?>" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= site_url('Login/salir')?>" class="btn btn-default btn-flat">Cerrar sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
