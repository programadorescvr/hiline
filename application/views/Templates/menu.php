<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 20/11/17
 * Time: 03:28 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="<?= site_url()?>"><i class="fa fa-link"></i> <span>Inicio</span></a></li>
            <li><a href="<?= site_url('Clientes')?>"><i class="fa fa-link"></i> <span>Clientes</span></a></li>
            <li><a href="<?= site_url('Clientes/Crear_cliente')?>"><i class="fa fa-link"></i> <span>Crear cliente</span></a></li>
            <!--<li class="treeview active">
                <a href="<?= site_url()?>"><i class="fa fa-link"></i> <span>Clientes</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Crear cliente</a></li>
                </ul>
            </li>-->
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
