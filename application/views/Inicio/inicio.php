<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 26/01/18
 * Time: 12:36 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Clientes</h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url()?>"><i class="fa fa-dashboard"></i> Inicio</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Inicio</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container-fluid">
                    <?=(isset($mensaje)) ? '<div class="row">'.$mensaje.'</div>':''?>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="info-box bg-yellow-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Clientes Totales</span>
                                        <span class="info-box-number"><?=$totales['n_clientes']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-blue-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Clientes Activos</span>
                                        <span class="info-box-number"><?=$totales['n_clientes_activos']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-aqua-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Total de las llamadas realizadas</span>
                                        <span class="info-box-number"><?=$totales['n_llamdas']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="info-box bg-green-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Duración total de las llamadas realizadas</span>
                                        <span class="info-box-number"><?=$totales['n_duracion']?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-offset-3 col-md-6">
                                <div class="info-box bg-orange-active">
                                    <span class="info-box-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="info-box-content">
                                        <span class="">Total de las llamadas pendientes</span>
                                        <span class="info-box-number"><?=$totales['n_llamadas_p']?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="text-center">Idiomas</h3>
                        <div class="col-md-offset-2 col-md-8">
                            <div class="owl-carousel owl-theme banderas">
                                <div class="item"><img src="<?= base_url('public/img/banderas/alemania.png') ?>" alt="aleman" ></div>
                                <div class="item"><img src="<?= base_url('public/img/banderas/españa.png') ?>" alt="español" ></div>
                                <div class="item"><img src="<?= base_url('public/img/banderas/estados-unidos.png') ?>" alt="Ingles estadounidence" ></div>
                                <div class="item"><img src="<?= base_url('public/img/banderas/francia.png') ?>" alt="Frances" ></div>
                                <div class="item"><img src="<?= base_url('public/img/banderas/inglaterra.png') ?>" alt="Ingles britanico" ></div>
                                <div class="item"><img src="<?= base_url('public/img/banderas/italia.png') ?>" alt="Italiano" ></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="text-center">Temáticas</h3>
                            <div class="graficas">
                                <canvas id="tematicas" height="100%" width="100%"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-center">Fortalezas</h3>
                            <div class="table-responsive">
                                <table class="fortalezas table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Fortalezas</th>
                                        <th>Ranking</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($fortalezas)) {
                                        foreach ($fortalezas as $item){
                                            ?>
                                            <tr>
                                                <td><?=$item['nombre_fortalezas']?></td>
                                                <td><?=$item['n_fortaleza']?> %</td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    var tematicas = document.getElementById("tematicas");
    var tematicas_chart = new Chart(tematicas,{
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    <?php foreach ($tematicas as $item) echo $item['n_tematicas'].', '?>
                ],backgroundColor: [
                    <?php foreach ($tematicas as $item) echo 'getRandomColor(), '?>
                ],
                label: 'Temáticas'
            }],
            labels: [
                <?php foreach ($tematicas as $item) echo '\''.$item['nombre_tematica'].'\', '?>
            ]
        },
        options: {
            responsive: true
        }
    });
</script>

