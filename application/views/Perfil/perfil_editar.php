<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 27/11/17
 * Time: 02:48 PM
 * Author: ideco.com.co
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Perfil</h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url('Perfil/Editar_perfil')?>"><i class="fa fa-dashboard"></i> Editar cliente</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Editar perfil</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <form action="<?= site_url('Perfil/Editar_perfil') ?>" method="post" class="form" id="form-editar-cliente">
                <div class="box-body">
                    <div class="container-fluid">
                        <?php if (!empty(validation_errors())) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h5><i class="icon fa fa-warning"></i>Alerta</h5>
                                        <h6><?= validation_errors()?></h6>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php
                        if (isset($mensaje)){
                            ?>
                            <div class="row"><?= $mensaje?></div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="nombres">Nombres</label>
                                        <input type="text" id="nombres" name="nombres" class="form-control" value="<?= set_value('nombres',$perfil['nombres'])?>"  required/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="apellidos">Apellidos</label>
                                        <input type="text" id="apellidos" name="apellidos" class="form-control" value="<?= set_value('apellidos',$perfil['apellidos'])?>" required/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="celular">Celular</label>
                                        <input type="number" id="celular" name="celular" class="form-control" required value="<?= set_value('celular',$perfil['celular'])?>"/>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="correo">Correo</label>
                                        <input type="email" id="correo" name="correo" class="form-control" required value="<?= set_value('correo',$perfil['correo'])?>"/>
                                    </div>
                                    <div class="col-md-12 text-center form-group">
                                        <!--<a href="<?= site_url('Perfil/Cambiar_contraseña') ?>" class="btn btn-primary"><i class="fa fa-save"></i> Contraseña</a>-->
                                        <!--<a href="#" class="btn btn-primary"><i class="fa fa-save"></i> Contraseña</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <a href="<?= site_url('') ?>" class="btn btn-default pull-left"><i class="fa fa-times"></i> Cancelar</a>
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-pencil"></i> Editar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
