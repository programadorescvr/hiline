<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 10:59 AM
 * Author: ideco.com.co
 */
class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

    }
    /** Iniciar sesión*/
    public function index()
    {
        //Validar campos requeridos
        $this->form_validation->set_rules('email', 'Correo', 'required');
        $this->form_validation->set_rules('password', 'Clave', 'required');

        if ($this->form_validation->run() === FALSE) {
            //En caso que los campos se manden vascios
            $this->load->view('Login/login');
        } else {

            //Obetener los valores de email y password
            $correo = $this->input->post('email');
            $clave = md5($this->input->post('password'));
            //Llamar los datos iguales de la base de datos
            $info_usuario = $this->tutores->autenticacion($correo, $clave);
            //Validar si existe el usuario
            if($info_usuario === FALSE || $info_usuario === NULL){
                //Mensaje de datos incorrectos
                $datos['mensaje']="Usuario o Contraseña Incorrecto<br>Por favor Intentelo de nuevo.";
                $this->load->view('Login/login',$datos);
            } else {
                //Asigar como variable de session los datos importantes
                $this->session->set_userdata('id_usuario', $info_usuario['id_tutor']);
                $this->session->set_userdata('nombre', $info_usuario['nombres']);
                $this->session->set_userdata('apellido', $info_usuario['apellidos']);
                $this->session->set_userdata('id_rol', $info_usuario['id_rol']);
                $this->session->set_userdata('exito', 0);

                //Guiarlo al inicio
                redirect('');
            }
        }
    }
    /** Fin Iniciar sesión*/

    /** Cerrar sesión*/
    public function salir(){
        $this->session->sess_destroy();
        //Redireccionando a la página principal del usuario.
        redirect('Login', 'location', 301);
    }
    /** Fin Cerrar sesión*/

}