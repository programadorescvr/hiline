<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 26/01/18
 * Time: 12:32 PM
 * Author: ideco.com.co
 */
class Inicio extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        //Datos para incluir en las vistas
        $titulo['titulo'] = 'Inicio';
        $id_tutor = $this->session->userdata('id_usuario');

        //Datos estandar
        $clientes = $this->clientes->ver_total_clientes($id_tutor);
        $datos['totales']['n_clientes'] = $clientes['n_clientes'];
        $datos['totales']['n_llamadas_p'] = $clientes['n_llamadas_p'];
        $datos['totales']['n_clientes_activos'] = $clientes['n_clientes_activos'];

        $llamadas = $this->reportes->ver_estandar_llamadas_total($id_tutor);
        $datos['totales']['n_llamdas'] = $llamadas['n_llamadas'];
        $datos['totales']['n_duracion'] = $llamadas['n_duracion'];

        //Grafiva Tematicas
        $datos['tematicas'] = $this->reportes->ver_tematicas_tutor($id_tutor);
        //Tabla fortalezas
        $datos['fortalezas'] = $this->fortalezas->ver_fortalezas_tutor($id_tutor);

        //Llamar la vista
        $this->load->view('Templates/header',$titulo);
        $this->load->view('Templates/menu');
        $this->load->view('Inicio/inicio',$datos);
        $this->load->view('Templates/footer');
    }
}