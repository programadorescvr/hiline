<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 27/11/17
 * Time: 02:38 PM
 * Author: ideco.com.co
 */
class Perfil extends MY_Controller{
    public function __construct()
    {
        parent::__construct();

    }

    public function Editar_perfil(){
        $id_tutor = $this->session->userdata('id_usuario');
        $datos['perfil'] = $this->tutores->ver_perfil($id_tutor);
        //Validar que esxista el prefil
        if (!empty($datos['perfil']) && $datos['perfil'] != null){
            //Validar campos requeridos
            $this->form_validation->set_rules('nombres', 'Nombre', 'required');
            $this->form_validation->set_rules('apellidos', 'Apellidos', 'required');
            $this->form_validation->set_rules('celular', 'Celular', 'required');
            $this->form_validation->set_rules('correo', 'Correo', 'required');

            if ($this->form_validation->run() === FALSE) {
                //Datos para incluir en las vistas
                $titulo['titulo'] = 'Editar perfil';
                $datos['id_tutor'] = $id_tutor;

                //Validar mensajes
                $varControlExito = $this->session->userdata('exito');
                switch ($varControlExito){
                    case 1:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "Contraseña editada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                        break;
                    case 2:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "No se edito la contraseña";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                        break;
                }

                //Llamar la vista
                $this->load->view('Templates/header', $titulo);
                $this->load->view('Templates/menu');
                $this->load->view('Perfil/perfil_editar', $datos);
                $this->load->view('Templates/footer');
            }else{
                //Organizar los datos para crear el cliente
                $envio['nombres'] = $this->input->post('nombres');
                $envio['apellidos'] = $this->input->post('apellidos');
                $envio['celular'] = $this->input->post('celular');
                $envio['correo'] = $this->input->post('correo');

                //Asignar nuevas variables de sesión
                $this->session->set_userdata('nombre', $envio['nombres']);
                $this->session->set_userdata('apellido', $envio['apellidos']);

                //Editar el perfil
                $id_perfil = $this->tutores->editar_perfil($id_tutor,$envio);
                //Validar si se edito
                if ($id_perfil != false){
                    //Edicion exitosa
                    $this->session->set_userdata('exito', 8);
                }else{
                    //Edicion fallida
                    $this->session->set_userdata('exito', 9);
                }
                redirect('');

            }
        }else{
            redirect('');
        }
    }

}