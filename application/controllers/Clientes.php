<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 20/11/17
 * Time: 12:39 PM
 * Author: ideco.com.co
 */
class Clientes extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
    }

    //Ver todos los clientes
    public function index()
    {
        //Validar mensajes
        $varControlExito = $this->session->userdata('exito');
        switch ($varControlExito){
            case 1:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "Cliente creado";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                break;
            case 2:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "No se creo el clinete";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                break;
            case 3:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "Cliente editado";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                break;
            case 4:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "No se edito el clinete";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                break;
            case 5:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "No se encontro el clinete";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                break;
            case 6:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "Cliente borrado";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                break;
            case 7:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "No se borro el clinete";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                break;
            case 8:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "Perfil editado";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                break;
            case 9:
                $this->session->set_userdata(array('exito' => 0));
                $mensaje['mensaje'] = "No se edito el perfil";
                $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                break;
        }

        //Datos para incluir en las vistas
        $titulo['titulo'] = 'Clientes';
        $datos['clientes'] = $this->clientes->ver_clientes();

        //Llamar la vista
        $this->load->view('Templates/header',$titulo);
        $this->load->view('Templates/menu');
        $this->load->view('Clientes/clientes_ver',$datos);
        $this->load->view('Templates/footer');
    }

    //Ver el cliente con su historial
    public function Ver_cliente($id_cliente)
    {
        //Validar que el id exista
        if (!empty($id_cliente)){
            //Llamar datos del cliente
            $datos['cliente'] = $this->clientes->ver_cliente($id_cliente);

            if (!empty($datos['cliente']) && $datos['cliente'] != null){

                //Datos para incluir en las vistas
                $titulo['titulo'] = 'Ver Cliente';
                $llamadas = $this->reportes->ver_registros_llamadas($id_cliente);
                $datos['llamadas'] = array();
                $n_llamadas = 0;
                foreach ($llamadas as $item){
                    $item['fecha_llamada'] = $this->formato_fecha_ver($item['fecha_llamada']);
                    array_push($datos['llamadas'],$item);
                    $n_llamadas++;
                }
                $datos['id_cliente'] = $id_cliente;
                $datos['tematicas'] = $this->reportes->ver_tematicas_cliente($id_cliente);

                $datos['fortalezas'] = $this->fortalezas->ver_fortalezas();
                $datos['fortalezas_cliente'] = $this->fortalezas->ver_fortalezas_cliente($id_cliente);
                $datos['categorias'] = $this->fortalezas->ver_categorias();

                $datos['seguimineto'] = $this->reportes->ver_estandar_llamadas_cliente($id_cliente);



                //Validar mensajes
                $varControlExito = $this->session->userdata('exito');
                switch ($varControlExito){
                    case 1:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "Llamada creada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                        break;
                    case 2:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "No se creo la llamada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                        break;
                    case 3:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "Llamada editada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                        break;
                    case 4:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "No se edito la llamada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                        break;
                    case 5:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "No se encontro la llamada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                        break;
                   case 6:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "Llamada borrada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_afirmativo',$mensaje,true);
                        break;
                    case 7:
                        $this->session->set_userdata(array('exito' => 0));
                        $mensaje['mensaje'] = "No se borro la llamada";
                        $datos['mensaje'] = $this->load->view('Templates/mensajes/mensaje_error',$mensaje,true);
                        break;
                }

                //Llamar la vista
                $this->load->view('Templates/header',$titulo);
                $this->load->view('Templates/menu');
                $this->load->view('Clientes/clientes_ver_perfil',$datos);
                $this->load->view('Templates/footer');
            }else{
                redirect('');
            }
        }else{
            redirect('');
        }

    }

    //Crear el cliente
    public  function Crear_cliente(){

        //Validar campos requeridos
        $this->form_validation->set_rules('nombres', 'Nombre', 'required');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'required');
        $this->form_validation->set_rules('celular', 'Celular', 'required');
        $this->form_validation->set_rules('correo', 'Correo', 'required');
        $this->form_validation->set_rules('tutor', 'Tutor', 'required');
        $this->form_validation->set_rules('enfoque', 'Enfoque', 'required');
        $this->form_validation->set_rules('horario', 'Horario', 'required');
        $this->form_validation->set_rules('fecha_inicio', 'Fecha inicio', 'required');
        $this->form_validation->set_rules('n_llamadas', 'N° Llamadas', 'required');

        if ($this->form_validation->run() === FALSE) {
            //Datos para incluir en las vistas
            $titulo['titulo'] = 'Crear cliente';
            $datos['id_tutor'] = $this->session->userdata('id_usuario');
            $datos['tutores'] = $this->tutores->listar_tutores();
            $datos['fecha'] = $this->fecha();

            //Llamar la vista
            $this->load->view('Templates/header',$titulo);
            $this->load->view('Templates/menu');
            $this->load->view('Clientes/clinetes_crear_perfil',$datos);
            $this->load->view('Templates/footer');
        } else {
            //Organizar los datos para crear el cliente
            $envio['nombres'] = $this->input->post('nombres');
            $envio['apellidos'] = $this->input->post('apellidos');
            $envio['celular'] = $this->input->post('celular');
            $envio['correo'] = $this->input->post('correo');
            $envio['id_tutor'] = $this->input->post('tutor');
            $envio['enfoque'] = $this->input->post('enfoque');
            $envio['horario_llamada'] = $this->input->post('horario');
            $envio['n_llamadas'] = $this->input->post('n_llamadas');
            $envio['fecha_inicio'] = $this->formato_fecha_guardar($this->input->post('fecha_inicio'));
            $envio['fecha_registro_s'] = $this->fecha_hora();

            //Crear el cliente
            $id_cliente = $this->clientes->crear_clinete($envio);
            //Validar si se creo
            if ($id_cliente != false){
                //Creación exitosa
                $this->session->set_userdata('exito', 1);
            }else{
                //Creación fallida
                $this->session->set_userdata('exito', 2);
            }
            redirect('');
        }
    }

    //Editar el cliente
    public function Editar_cliente($id_cliente)
    {
        //Validar que el id exista
        if (!empty($id_cliente)){
            //Llamar datos del cliente
            $datos['cliente'] = $this->clientes->ver_cliente($id_cliente);
            $datos['cliente']['fecha_inicio'] = $this->formato_fecha_editar($datos['cliente']['fecha_inicio']);

            if (!empty($datos['cliente']) && $datos['cliente'] != null){

                //Validar campos requeridos
                $this->form_validation->set_rules('nombres', 'Nombre', 'required');
                $this->form_validation->set_rules('apellidos', 'Apellidos', 'required');
                $this->form_validation->set_rules('celular', 'Celular', 'required');
                $this->form_validation->set_rules('correo', 'Correo', 'required');
                $this->form_validation->set_rules('tutor', 'Tutor', 'required');
                $this->form_validation->set_rules('enfoque', 'Enfoque', 'required');
                $this->form_validation->set_rules('horario', 'Horario', 'required');
                $this->form_validation->set_rules('fecha_inicio', 'Fecha inicio', 'required');
                $this->form_validation->set_rules('n_llamadas', 'N° Llamadas', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Editar Cliente';
                    $datos['tutores'] = $this->tutores->listar_tutores();

                    //Llamar la vista
                    $this->load->view('Templates/header',$titulo);
                    $this->load->view('Templates/menu');
                    $this->load->view('Clientes/clinetes_editar_perfil',$datos);
                    $this->load->view('Templates/footer');
                }else{
                    //Organizar los datos para crear el cliente
                    $envio['nombres'] = $this->input->post('nombres');
                    $envio['apellidos'] = $this->input->post('apellidos');
                    $envio['celular'] = $this->input->post('celular');
                    $envio['correo'] = $this->input->post('correo');
                    $envio['id_tutor'] = $this->input->post('tutor');
                    $envio['enfoque'] = $this->input->post('enfoque');
                    $envio['horario_llamada'] = $this->input->post('horario');
                    $envio['n_llamadas'] = $this->input->post('n_llamadas');
                    $envio['fecha_inicio'] = $this->formato_fecha_guardar($this->input->post('fecha_inicio'));
                    $envio['fecha_registro_s'] = $this->fecha_hora();

                    //Editar el cliente
                    $id_cliente = $this->clientes->editar_cliente($id_cliente,$envio);
                    //Validar si se creo
                    if ($id_cliente != false){
                        //Edicion exitosa
                        $this->session->set_userdata('exito', 3);
                    }else{
                        //Edicion fallida fallida
                        $this->session->set_userdata('exito', 4);
                    }
                    redirect('');
                }
            }else{
                //No se encontro cliente
                $this->session->set_userdata('exito', 5);
                redirect('');
            }
        }else{
            //No se encontro cliente
            $this->session->set_userdata('exito', 5);
            redirect('');
        }

    }
    //Borrar el cliente con su historial
    public function Borrar_cliente($id_cliente)
    {
        //Validar que el id exista
        if (!empty($id_cliente)){
            //Llamar datos del cliente
            $datos['cliente'] = $this->clientes->ver_cliente($id_cliente);

            if (!empty($datos['cliente']) && $datos['cliente'] != null){

                //Validar campos requeridos
                $this->form_validation->set_rules('borrar', 'Nombre', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar Cliente';
                    $datos['tutores'] = $this->tutores->listar_tutores();

                    //Llamar la vista
                    $this->load->view('Templates/header',$titulo);
                    $this->load->view('Templates/menu');
                    $this->load->view('Clientes/Clientes_borrar_perfil',$datos);
                    $this->load->view('Templates/footer');
                }else{
                    //Organizar los datos para crear el cliente
                    $confirmacion= $this->input->post('borrar');
                    //validar el borar
                    if ($confirmacion === 'BORRAR'){
                        //Borra el cliente
                        $id_cliente = $this->clientes->borrar_cliente($id_cliente);
                        if ($id_cliente != false){
                            //Borrado exitoso
                            $this->session->set_userdata('exito', 6);
                        }else{
                            //Borrado fallida
                            $this->session->set_userdata('exito', 7);
                        }
                    }else{
                        //Borrado fallido
                        $this->session->set_userdata('exito', 7);
                    }


                    redirect('');
                }
            }else{
                //No se encontro cliente
                $this->session->set_userdata('exito', 5);
                redirect('');
            }
        }else{
            //No se encontro cliente
            $this->session->set_userdata('exito', 5);
            redirect('');
        }

    }

    //Crear la llamada del cliente
    public  function Crear_llamada($id_cliente){

        //Validar que el id exista
        if(!empty($id_cliente)){
            //Llamar datos del cliente
            $datos['cliente'] = $this->clientes->ver_cliente($id_cliente);

            if (!empty($datos['cliente']) && $datos['cliente'] != null){

                //Validar campos requeridos
                $this->form_validation->set_rules('tutor', 'Tutor', 'required');
                $this->form_validation->set_rules('duracion', 'Duración', 'required');
                $this->form_validation->set_rules('fecha_llamada', 'Fecha llamada', 'required');
                $this->form_validation->set_rules('hora_llamada', 'Hora llamada', 'required');
                $this->form_validation->set_rules('tematica', 'Tematica', 'required');
                $this->form_validation->set_rules('recomendaciones', 'Recomendaciones', 'required');

                if ($this->input->post('tematica') == 'otros'){
                    $this->form_validation->set_rules('otros', 'Otros', 'required');
                }

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Crear cliente';
                    $datos['id_tutor'] = $this->session->userdata('id_usuario');
                    $datos['tutores'] = $this->tutores->listar_tutores();
                    $datos['tematicas'] = $this->tematicas->ver_tematicas();
                    $datos['fortalezas'] = $this->fortalezas->ver_fortalezas();
                    $datos['categorias'] = $this->fortalezas->ver_categorias();
                    $datos['fecha'] = $this->fecha();
                    $datos['hora'] = $this->hora();
                    $datos['id_cliente'] = $id_cliente;


                    //Llamar la vista
                    $this->load->view('Templates/header',$titulo);
                    $this->load->view('Templates/menu');
                    $this->load->view('Clientes/llamadas_crear',$datos);
                    $this->load->view('Templates/footer');
                } else {
                    //validar tematica
                    if ($this->input->post('tematica') == "otros"){
                        $otros['nombre_tematica'] = $this->input->post('otros');
                        $id_ortros = $this->tematicas->crear_tematica($otros);
                        $envio['id_tematica'] = $id_ortros;

                    }else{
                        $envio['id_tematica'] = $this->input->post('tematica');

                    }
                    //Organizar los datos para crear la llamada
                    $envio['id_cliente'] = $id_cliente;
                    $envio['id_tutor'] = $this->input->post('tutor');
                    $envio['recomendaciones'] = $this->input->post('recomendaciones');
                    $envio['duracion'] = $this->input->post('duracion');
                    $envio['fecha_llamada'] = $this->formato_fecha_hora_guardar($this->input->post('fecha_llamada'),$this->input->post('hora_llamada'));
                    $envio['fecha_registro_s'] = $this->fecha_hora();

                    //Crear la llamada
                    $id_llamada = $this->reportes->crear_llamada($envio);

                    //Arreglar fortaleza llamadas
                    $fortalezas = $this->input->post('fortalezas');
                    if (!empty($fortalezas)){
                        $envio2 = array();
                        foreach ($fortalezas as $item){
                            array_push($envio2,array('fk_fortalezas' => $item, 'fK_llamadas' => $id_llamada));
                        }
                        $this->fortalezas->guardar_fortalezas_llamadas($envio2);
                    }
                    //Validar si se creo
                    if ($id_llamada != false){
                        //Creación exitosa
                        $llamada = 'n_llamadas-1';
                        $this->clientes->editar_cliente_n_llamadas($id_cliente,$llamada);
                        $this->session->set_userdata('exito', 1);
                    }else{
                        //Creación fallida
                        $this->session->set_userdata('exito', 2);
                    }
                    redirect('Clientes/Ver_cliente/'.$id_cliente);
                }
            }else{
                //No se encontro cliente
                $this->session->set_userdata('exito', 5);
                redirect('');
            }
        }
    }

    //Editar el llamada
    public function Editar_llamada($id_llamada)
    {
        //Validar que el id exista
        if (!empty($id_llamada)){
            //Llamar datos del cliente
            $datos['llamada'] = $this->reportes->ver_llamada($id_llamada);
            $datos['llamada']['fecha'] = $this->formato_fecha_editar($datos['llamada']['fecha_llamada']);
            $datos['llamada']['hora'] = $this->formato_hora_editar($datos['llamada']['fecha_llamada']);
            $datos['llamada']['fortalezas'] = $this->fortalezas->ver_fortalezas_llamada($id_llamada);

            if (!empty($datos['llamada']) && $datos['llamada'] != null){

                //Validar campos requeridos
                $this->form_validation->set_rules('tutor', 'Tutor', 'required');
                $this->form_validation->set_rules('duracion', 'Duración', 'required');
                $this->form_validation->set_rules('fecha_llamada', 'Fecha llamada', 'required');
                $this->form_validation->set_rules('hora_llamada', 'Hora llamada', 'required');
                $this->form_validation->set_rules('tematica', 'Tematica', 'required');
                $this->form_validation->set_rules('recomendaciones', 'Recomendaciones', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Editar Llamada';
                    $datos['tutores'] = $this->tutores->listar_tutores();
                    $datos['tematicas'] = $this->tematicas->ver_tematicas();
                    $datos['fortalezas'] = $this->fortalezas->ver_fortalezas();
                    $datos['categorias'] = $this->fortalezas->ver_categorias();


                    //Llamar la vista
                    $this->load->view('Templates/header',$titulo);
                    $this->load->view('Templates/menu');
                    $this->load->view('Clientes/llamadas_editar',$datos);
                    $this->load->view('Templates/footer');
                }else{
                    //validar tematica
                    if ($this->input->post('tematica') == "otros"){
                        $otros['nombre_tematica'] = $this->input->post('otros');
                        $id_ortros = $this->tematicas->crear_tematica($otros);
                        $envio['id_tematica'] = $id_ortros;

                    }else{
                        $envio['id_tematica'] = $this->input->post('tematica');

                    }
                    //Organizar los datos para crear la llamada
                    $envio['id_tutor'] = $this->input->post('tutor');
                    $envio['recomendaciones'] = $this->input->post('recomendaciones');
                    $envio['duracion'] = $this->input->post('duracion');
                    $envio['fecha_llamada'] = $this->formato_fecha_hora_guardar($this->input->post('fecha_llamada'),$this->input->post('hora_llamada'));

                    //Editar la llamada
                    $id_llamada_c = $this->reportes->editar_llamada($id_llamada,$envio);

                    //Arreglar fortaleza llamadas
                    $fortalezas = $this->input->post('fortalezas');
                    if (!empty($fortalezas)){
                        $envio2 = array();
                        foreach ($fortalezas as $item){
                            array_push($envio2,array('fk_fortalezas' => $item, 'fK_llamadas' => $id_llamada));
                        }
                        $this->fortalezas->borrar_fortalezas_llamadas($id_llamada);
                        $this->fortalezas->guardar_fortalezas_llamadas($envio2);
                    }
                    //Validar si se edito
                    if ($id_llamada_c != false){
                        //Edicion exitosa
                        $this->session->set_userdata('exito', 3);
                    }else{
                        //Edicion fallida fallida
                        $this->session->set_userdata('exito', 4);
                    }

                    redirect('Clientes/Ver_cliente/'.$datos['llamada']['id_cliente']);
                }
            }else{
                //No se encontro la llamada
                $this->session->set_userdata('exito', 5);
                redirect('Clientes/Ver_cliente/'.$datos['llamada']['id_cliente']);
            }
        }else{
            //No se encontro cliente
            $this->session->set_userdata('exito', 5);
            redirect('');
        }

    }

    //Borrar el cliente con su historial
    public function Borrar_llamada($id_llamada)
    {
        //Validar que el id exista
        if (!empty($id_llamada)){
            //Llamar datos del cliente
            $datos['llamada'] = $this->reportes->ver_llamada($id_llamada);

            if (!empty($datos['llamada']) && $datos['llamada'] != null){

                //Validar campos requeridos
                $this->form_validation->set_rules('borrar', 'Nombre', 'required');

                if ($this->form_validation->run() === FALSE) {
                    //Datos para incluir en las vistas
                    $titulo['titulo'] = 'Eliminar Llamada';
                    $datos['tutores'] = $this->tutores->listar_tutores();

                    //Llamar la vista
                    $this->load->view('Templates/header',$titulo);
                    $this->load->view('Templates/menu');
                    $this->load->view('Clientes/llamadas_borrar',$datos);
                    $this->load->view('Templates/footer');
                }else{
                    //Organizar los datos para crear el cliente
                    $confirmacion = $this->input->post('borrar');
                    //validar el borar
                    if ($confirmacion === 'BORRAR'){
                        //Borra el cliente
                        $id_llamada_c = $this->reportes->borrar_llamada($id_llamada);
                        if ($id_llamada_c != false){
                            //Borrado exitoso
                            $llamada = 'n_llamadas+1';
                            $this->clientes->editar_cliente_n_llamadas($datos['llamada']['id_cliente'],$llamada);
                            $this->session->set_userdata('exito', 6);
                        }else{
                            //Borrado fallida
                            $this->session->set_userdata('exito', 7);
                        }
                    }else{
                        //Borrado fallido
                        $this->session->set_userdata('exito', 7);
                    }
                    redirect('Clientes/Ver_cliente/'.$datos['llamada']['id_cliente']);
                }
            }else{
                //No se encontro cliente
                $this->session->set_userdata('exito', 5);
                redirect('');
            }
        }else{
            //No se encontro cliente
            $this->session->set_userdata('exito', 5);
            redirect('');
        }

    }
    //Ver el cliente con su historial
    public function Ver_llamada($id_llamada)
    {
        //Validar que el id exista
        if (!empty($id_llamada)){
            //Llamar datos del cliente
            $datos['llamada'] = $this->reportes->ver_llamada($id_llamada);
            $datos['llamada']['fortalezas'] = $this->fortalezas->ver_fortalezas_llamada($id_llamada);

            if (!empty($datos['llamada']) && $datos['llamada'] != null){

                //Datos para incluir en las vistas
                $titulo['titulo'] = 'Ver llamada';
                $datos['fortalezas'] = $this->fortalezas->ver_fortalezas();
                $datos['categorias'] = $this->fortalezas->ver_categorias();

                //Llamar la vista
                $this->load->view('Templates/header',$titulo);
                $this->load->view('Templates/menu');
                $this->load->view('Clientes/llamadas_ver',$datos);
                $this->load->view('Templates/footer');
            }else{
                //No se encontro cliente
                $this->session->set_userdata('exito', 5);
                redirect('');
            }
        }else{
            //No se encontro cliente
            $this->session->set_userdata('exito', 5);
            redirect('');
        }

    }




}