<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 11:17 AM
 * Author: ideco.com.co
 */
class Clientes_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    //Permite ver los datos del cliente
    public function ver_cliente($id_cliente){
        $this->db->select('clientes.id_cliente');
        $this->db->select('CONCAT(clientes.nombres, \' \', clientes.apellidos) As nombre_cliente');
        $this->db->select('clientes.nombres');
        $this->db->select('clientes.n_llamadas');
        $this->db->select('clientes.apellidos');
        $this->db->select('clientes.celular');
        $this->db->select('clientes.enfoque');
        $this->db->select('clientes.horario_llamada');
        $this->db->select('clientes.correo');
        $this->db->select('clientes.fecha_inicio');
        $this->db->select('CONCAT(tutores.nombres, \' \', tutores.apellidos) as nombre_tutor');
        $this->db->select('clientes.id_tutor');
        $this->db->join('tutores','clientes.id_tutor = tutores.id_tutor','inner');
        $this->db->where('clientes.id_cliente = '.$id_cliente);
        $query = $this->db->get('clientes');
        return $query->row_array();
    }

    //Crear un clinete
    public function crear_clinete($envio){
        $this->db->insert('clientes', $envio);
        $id =$this->db->insert_id();
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Permite llamar todos los clientes con su ultima llamada
    public function ver_clientes(){
        $this->db->select('clientes.id_cliente');
        $this->db->select('CONCAT(clientes.nombres, \' \', clientes.apellidos) As nombre_cliente');
        $this->db->select('clientes.enfoque');
        $this->db->select('clientes.n_llamadas');
        $this->db->select('llamadas.fecha');
        $this->db->select('llamadas.llamadas');
        $this->db->select('clientes.horario_llamada');
        $this->db->select('CONCAT(tutores.nombres, \' \', tutores.apellidos) as nombre_tutor');
        $this->db->join('tutores','clientes.id_tutor = tutores.id_tutor','inner');
        $this->db->join('(SELECT id_cliente,max(fecha_llamada) as fecha, count(id_cliente) as llamadas FROM reporte_llamadas group by reporte_llamadas.id_cliente) llamadas','llamadas.id_cliente = clientes.id_cliente','left');
        $query = $this->db->get('clientes');
        return $query->result_array();
    }

    //Editar el cliente
    public function editar_cliente($id_cliente,$envio)
    {
        $this->db->where('id_cliente', $id_cliente);
        $id = $this->db->update('clientes', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }
    //Editar el cliente - numero de llamadas
    public function editar_cliente_n_llamadas($id_cliente,$envio)
    {
        $this->db->set('n_llamadas', $envio,false);
        $this->db->where('id_cliente', $id_cliente);
        $id = $this->db->update('clientes');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }
    //Borrar cliente
    public function borrar_cliente($id_cliente){
        $this->db->where('clientes.id_cliente', $id_cliente);
        $id = $this->db->delete('clientes');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //Hallar cuantos clientes existen y la bolsa de llamadas
    public function ver_total_clientes($id_tutor){
        $this->db->select('count(clientes.id_cliente) as n_clientes');
        $this->db->select('sum(if(clientes.n_llamadas > 0,1,0)) as n_clientes_activos');
        $this->db->select('sum(clientes.n_llamadas) as n_llamadas_p');
        $this->db->where('clientes.id_tutor = '.$id_tutor);
        $query = $this->db->get('clientes');
        return $query->row_array();
    }


}