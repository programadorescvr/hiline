<?php
/**
 * Created by PhpStorm.
 * User: Cesar Alejandro Antolinez
 * Date: 11/01/18
 * Time: 09:57 AM
 * Author: ideco.com.co
 */
class Fortalezas_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    //ver todas las fortalezas
    public function ver_fortalezas(){
        $this->db->select('*');
        $query = $this->db->get('fortalezas');
        return $query->result_array();
    }

    //Ver todas las categorias
    public function ver_categorias(){
        $this->db->select('*');
        $query = $this->db->get('categorias');
        return $query->result_array();
    }
    //Ver todas las fortalezas de una llamada
    public function ver_fortalezas_llamada($id_llamada){
        $this->db->select('fortalezas_llamadas.*');
        $this->db->select('fortalezas.nombre_fortalezas');
        $this->db->select('fortalezas.fk_categorias');
        $this->db->where('fortalezas_llamadas.fK_llamadas', $id_llamada);
        $this->db->join('fortalezas','fortalezas_llamadas.fk_fortalezas = fortalezas.id_fortalezas','left');
        $query = $this->db->get('fortalezas_llamadas');
        return $query->result_array();
    }

    //Ingresar las fortalezas de la llamada
    public function guardar_fortalezas_llamadas($envio){
        $this->db->insert_batch('fortalezas_llamadas', $envio);
        $id =$this->db->insert_id();
        if (!empty($id) && $id != null && $id != false )
        {
            return true;
        }else{
            return false;
        }
    }

    //borrar las fortalezas de la llamada
    public function borrar_fortalezas_llamadas($id_llamada){
        $this->db->where('fortalezas_llamadas.fK_llamadas', $id_llamada);
        $id = $this->db->delete('fortalezas_llamadas');
        if (!empty($id) && $id != null && $id != false )
        {
            return true;
        }else{
            return false;
        }
    }
    //Que fortalezas tiene el cliente
    public function ver_fortalezas_cliente($id_cliente){
        $this->db->select('fk_fortalezas');
        $this->db->join('reporte_llamadas','fortalezas_llamadas.fK_llamadas = reporte_llamadas.id_reporte','left');
        $this->db->where('reporte_llamadas.id_cliente = '.$id_cliente);
        $this->db->group_by("fk_fortalezas");
        $query = $this->db->get('fortalezas_llamadas');
        return $query->result_array();
    }

    //Consulta para la grafica de tematicas por tutor
    public function ver_fortalezas_tutor($id_tutor){
        $this->db->select('concat(categorias.nombre_categorias,\'-\',fortalezas.nombre_fortalezas) as nombre_fortalezas');
        $this->db->select('round((count(fortalezas_llamadas.id_fortalezas_llamadas)*100)/(SELECT count(reporte_llamadas.id_reporte) as n_tematicas from `hi-line`.reporte_llamadas inner join clientes on reporte_llamadas.id_cliente = clientes.id_cliente where clientes.id_tutor = '.$id_tutor.'),2) as n_fortaleza');

        $this->db->join('reporte_llamadas','fortalezas_llamadas.fK_llamadas = reporte_llamadas.id_reporte','inner');
        $this->db->join('clientes','reporte_llamadas.id_cliente = clientes.id_cliente','inner');

        $this->db->join('fortalezas','fortalezas_llamadas.fk_fortalezas = fortalezas.id_fortalezas','left');
        $this->db->join('categorias','fortalezas.fk_categorias = categorias.id_categorias','inner');

        $this->db->where('clientes.id_tutor = '.$id_tutor);

        $this->db->group_by("fortalezas_llamadas.fk_fortalezas");
        $query = $this->db->get('fortalezas_llamadas');
        return $query->result_array();
    }


}