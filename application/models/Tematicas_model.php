<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 09/01/18
 * Time: 02:10 PM
 * Author: ideco.com.co
 */
class Tematicas_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    //Ver todas la tematicas
    public function ver_tematicas(){
    	$this->db->select('*');
        $query = $this->db->get('tematicas');
        return $query->result_array();
    }
    //Ver todas la tematicas
    public function crear_tematica($envio){
        $this->db->insert('tematicas', $envio);
        $id =$this->db->insert_id();
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }


}