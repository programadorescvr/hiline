<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 11:19 AM
 * Author: ideco.com.co
 */
class Tutores_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }
    //Función que Autentica a un usuario
    public function autenticacion($correo, $clave)
    {
        if (($correo === FALSE) or ($clave === FALSE)) {
            return FALSE;
        } else {
            $this->db->select('*');
            $this->db->where('correo', $correo);
            $this->db->where('contraseña', $clave);
            $query = $this->db->get('tutores');
            return $query->row_array();
        }
    }
    //Lista todos los tutores
    public function listar_tutores(){
        $this->db->select('tutores.id_tutor');
        $this->db->select('CONCAT(tutores.nombres, \' \', tutores.apellidos) as nombre_tutor');
        $query = $this->db->get('tutores');
        return $query->result_array();
    }
    //Ver tutor
    public function ver_perfil($id_tutor){
        $this->db->select('tutores.*');
        $this->db->where('tutores.id_tutor = '.$id_tutor);
        $query = $this->db->get('tutores');
        return $query->row_array();
    }
    //Editar perfil
    public function editar_perfil($id_perfil,$envio)
    {
        $this->db->where('tutores.id_tutor', $id_perfil);
        $id = $this->db->update('tutores', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }
}