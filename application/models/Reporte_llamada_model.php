<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 21/11/17
 * Time: 02:45 PM
 * Author: ideco.com.co
 */
class Reporte_llamada_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    //Permite ver todas las llamadas de un cliente
    public function ver_registros_llamadas($id_cliente){
        $this->db->select('reporte_llamadas.id_reporte');
        $this->db->select('CONCAT(tutores.nombres, \' \', tutores.apellidos) as nombre_tutor');
        $this->db->select('reporte_llamadas.id_tematica');
        $this->db->select('reporte_llamadas.recomendaciones');
        $this->db->select('reporte_llamadas.fecha_llamada');
        $this->db->select('reporte_llamadas.duracion');
        $this->db->select('tematicas.nombre_tematica');
        $this->db->join('tutores','reporte_llamadas.id_tutor = tutores.id_tutor','inner');
        $this->db->join('tematicas','reporte_llamadas.id_tematica = tematicas.id_tematica','inner');
        $this->db->where('reporte_llamadas.id_cliente = '.$id_cliente);
        $query = $this->db->get('reporte_llamadas');
        return $query->result_array();
    }

    //Crear la llamada
    public function crear_llamada($envio){
        $this->db->insert('reporte_llamadas', $envio);
        $id =$this->db->insert_id();
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }
    //Editar la llamada
    public function editar_llamada($id_llamada,$envio)
    {
        $this->db->where('id_reporte', $id_llamada);
        $id = $this->db->update('reporte_llamadas', $envio);
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }

    //ver la llamda seleccionada
    public function ver_llamada($id_llamada){
        $this->db->select('reporte_llamadas.id_reporte');
        $this->db->select('CONCAT(tutores.nombres, \' \', tutores.apellidos) as nombre_tutor');
        $this->db->select('reporte_llamadas.id_tutor');
        $this->db->select('CONCAT(clientes.nombres, \' \', clientes.apellidos) as nombre_cliente');
        $this->db->select('reporte_llamadas.id_cliente');
        $this->db->select('reporte_llamadas.id_tematica');
        $this->db->select('tematicas.nombre_tematica as tematica');
        $this->db->select('reporte_llamadas.recomendaciones');
        $this->db->select('reporte_llamadas.fecha_llamada');
        $this->db->select('reporte_llamadas.duracion');
        
        

        $this->db->join('tutores','reporte_llamadas.id_tutor = tutores.id_tutor','inner');
        $this->db->join('clientes','reporte_llamadas.id_cliente = clientes.id_cliente','inner');
        $this->db->join('tematicas','reporte_llamadas.id_tematica = tematicas.id_tematica','inner');
        $this->db->where('reporte_llamadas.id_reporte = '.$id_llamada);
        $query = $this->db->get('reporte_llamadas');
        return $query->row_array();
    }

    //Eliminar llamadas
    public function borrar_llamada($id_llamada){
        $this->db->where('reporte_llamadas.id_reporte', $id_llamada);
        $id = $this->db->delete('reporte_llamadas');
        if (!empty($id) && $id != null && $id != false )
        {
            return $id;
        }else{
            return false;
        }
    }
    //Consulta para la grafica de tematicas por cliente
    public function ver_tematicas_cliente($id_cliente){
        $this->db->select('round((count(reporte_llamadas.id_reporte)*100)/(SELECT count(reporte_llamadas.id_reporte) as n_tematicas from `hi-line`.reporte_llamadas where reporte_llamadas.id_cliente = '.$id_cliente.'),2) as n_tematicas');
        $this->db->select('tematicas.nombre_tematica');

        $this->db->join('tematicas','reporte_llamadas.id_tematica = tematicas.id_tematica','inner');
        $this->db->where('reporte_llamadas.id_cliente = '.$id_cliente);

        $this->db->group_by("reporte_llamadas.id_tematica");
        $query = $this->db->get('reporte_llamadas');
        return $query->result_array();
    }
    //Consulta para saber el nunero, promedio, minimo y maximo de llamadas
    public function ver_estandar_llamadas_cliente($id_cliente){
        $this->db->select('count(reporte_llamadas.id_reporte) as n_llamdas');
        $this->db->select('min(reporte_llamadas.duracion) as min_llamada');
        $this->db->select('max(reporte_llamadas.duracion) as max_llamada');
        $this->db->select('round(avg(reporte_llamadas.duracion)) as avg_llamada');
        $this->db->where('reporte_llamadas.id_cliente = '.$id_cliente);
        $query = $this->db->get('reporte_llamadas');
        return $query->row_array();
    }
    //Consulta para saber el nunero de llamadas y la duracion total de las llamadas
    public function ver_estandar_llamadas_total($id_tutor){
        $this->db->select('count(reporte_llamadas.id_reporte) as n_llamadas');
        $this->db->select('sum(reporte_llamadas.duracion) as n_duracion');
        $this->db->join('clientes','reporte_llamadas.id_cliente = clientes.id_cliente','inner');
        $this->db->where('clientes.id_tutor = '.$id_tutor);
        $query = $this->db->get('reporte_llamadas');
        return $query->row_array();
    }
    //Consulta para la grafica de tematicas por tutor
    public function ver_tematicas_tutor($id_tutor){
        $this->db->select('round((count(reporte_llamadas.id_reporte)*100)/(SELECT count(reporte_llamadas.id_reporte) as n_tematicas from `hi-line`.reporte_llamadas inner join clientes on reporte_llamadas.id_cliente = clientes.id_cliente where clientes.id_tutor = '.$id_tutor.'),2) as n_tematicas');
        $this->db->select('tematicas.nombre_tematica');

        $this->db->join('tematicas','reporte_llamadas.id_tematica = tematicas.id_tematica','inner');
        $this->db->join('clientes','reporte_llamadas.id_cliente = clientes.id_cliente','inner');

        $this->db->where('clientes.id_tutor = '.$id_tutor);

        $this->db->group_by("reporte_llamadas.id_tematica");
        $query = $this->db->get('reporte_llamadas');
        return $query->result_array();
    }
}