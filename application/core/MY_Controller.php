<?php
/**
 * Created by PhpStorm.
 * User: Cesar Antolinez
 * Date: 20/11/17
 * Time: 12:30 PM
 * Author: ideco.com.co
 */

class MY_Controller extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        //Llamar el id session


        date_default_timezone_set('America/Bogota');

    }

    public function hora(){
        /** Toma la fecha para imprimir en pantalla con formato de 12 horas*/
        return (new \DateTime())->format('h:i A');
    }
    public function fecha(){
        /** Toma la fecha para imprimir en pantalla YYYY-MM-DD*/
        return (new \DateTime())->format('d-m-Y');
    }
    public function fecha_hora(){
        /** Toma la fecha y hora actual lista para almacenar en base de datos*/
        return (new \DateTime())->format('Y-m-d H:i:s');
    }
    public function formato_fecha_guardar($fecha){
        /** Toma la fecha y la tranforma en YYYY-MM-DD*/
        return date('Y-m-d', strtotime($fecha));
    }
    public function formato_fecha_hora_guardar($fecha,$hora){
        /** Toma la fecha y la tranforma en YYYY-MM-DD*/
        return date('Y-m-d H:i:s', strtotime($fecha.' '.$hora));
    }
    public function formato_fecha_editar($fecha){
        /** Toma la fecha y la tranforma en DD-MM-YYYY*/
        return date('d-m-Y', strtotime($fecha));
    }
    public function formato_hora_editar($hora){
        /** Toma la fecha y la tranforma en DD-MM-YYYY*/
        return date('h:i A', strtotime($hora));
    }
    public function formato_fecha_ver($fecha){
        /** Toma la fecha y la tranforma en DD-MM-YYYY*/
        return date('d-m-Y h:i A', strtotime($fecha));
    }

}