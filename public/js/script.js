/**
 * Created by Cesar Antolinez on 24/11/17.
 * Author : ideco.com.co
 */
//Pligun para la fecha
$('.fecha').datepicker({
    autoclose: true,
    format: 'dd-mm-yyyy'
});

//Plugin datatable
$.fn.dataTable.moment( 'DD-MM-YYYY hh:mm A');
$('.datatable').DataTable({
    "language": {
        "sLengthMenu": "Mostrar _MENU_ registros",
        "search": "Buscar:",
        "infoEmpty": "No existen datos.",
        "sInfo": "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "paginate": {
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    "order": [ 0, "desc" ],
});
$('.fortalezas').DataTable({
    "language": {
        "sLengthMenu": "Mostrar _MENU_ registros",
        "search": "Buscar:",
        "infoEmpty": "No existen datos.",
        "sInfo": "Mostrando _START_ al _END_ de _TOTAL_ registros",
        "paginate": {
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    "paging":   false,
    "order": [[ 1, "desc" ]]
});
//Plugin para la hora
$(".hora").timepicker({
    minuteStep: 1 ,
    use24hours: true,
    pick12HourFormat: false,
    showInputs: false
});

//Plugin para el multiselect
$('.multiple-select').select2();
//Desbloquear otros en tematica
$('#tematica').change(function (e) {
    var value = $('#tematica').val();
    var otros = $('#otros');
    if (value == 'otros'){
        otros.removeAttr('disabled');
    }else {
        otros.attr('disabled','disabled');
    }
});
//Carrusel de banderas
$('.banderas').owlCarousel({
    items:4,
    dots: false,
    loop:true,
    margin:10,
    autoplay:true,
    nav:false,
    autoplayTimeout:1500,
    autoplayHoverPause:true,
    responsive:{
        900:{
            items:5
        },
        700:{
            items:4
        },
        600:{
            items:2
        },
        300:{
            items:1
        }
    }
});
//randon color
/*function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}*/
