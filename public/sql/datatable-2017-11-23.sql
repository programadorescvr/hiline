-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: hi-line
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calificaciones`
--

DROP TABLE IF EXISTS `calificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificaciones` (
  `id_calificacion` int(11) NOT NULL AUTO_INCREMENT,
  `valor_calificacion` int(11) NOT NULL,
  PRIMARY KEY (`id_calificacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificaciones`
--

LOCK TABLES `calificaciones` WRITE;
/*!40000 ALTER TABLE `calificaciones` DISABLE KEYS */;
INSERT INTO `calificaciones` VALUES (1,1),(2,2),(3,3),(4,4),(5,5);
/*!40000 ALTER TABLE `calificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `celular` bigint(11) NOT NULL,
  `nombres` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellidos` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `enfoque` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `horario_llamada` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_registro_s` datetime NOT NULL,
  PRIMARY KEY (`id_cliente`),
  UNIQUE KEY `celular` (`celular`),
  UNIQUE KEY `correo` (`correo`),
  KEY `id_tutor` (`id_tutor`),
  CONSTRAINT `fk_clientes_1` FOREIGN KEY (`id_tutor`) REFERENCES `tutores` (`id_tutor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'correo1@hiline.com',3204321819,'nombre uno','apellido uno','Enfoque 1',1,'7 am - 10 pm','2017-01-15','2017-01-15 12:00:00'),(2,'correo2@hiline.com',3204321817,'nombre dos','apellido dos','Enfoque 2',1,'7 am - 10 pm','2017-01-15','2017-01-15 12:00:00'),(3,'correo3@hiline.com',3204321816,'nombre tres','apellido tres','Enfoque 3',1,'7 am - 10 pm','2017-01-15','2017-01-15 12:00:00'),(4,'correo4@hiline.com',3204321815,'nombre cuatro','apellido cuatro','Enfoque 4',1,'7 am - 10 pm','2017-01-15','2017-01-15 12:00:00'),(5,'correo5@hiline.com',3204321814,'nombre cinco','apellido cinco','Enfoque 5',1,'7 am - 10 pm','2017-01-15','2017-01-15 12:00:00');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporte_llamadas`
--

DROP TABLE IF EXISTS `reporte_llamadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporte_llamadas` (
  `id_reporte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_calificacion` int(11) NOT NULL,
  `tematica` varchar(400) COLLATE utf8mb4_spanish_ci NOT NULL,
  `recomendaciones` varchar(400) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_registro_s` date NOT NULL,
  `fecha_llamada` date NOT NULL,
  `duracion` int(11) NOT NULL,
  PRIMARY KEY (`id_reporte`),
  KEY `id_cliente1` (`id_cliente`),
  KEY `id_tutor2` (`id_tutor`),
  KEY `id_calificacion1` (`id_calificacion`),
  CONSTRAINT `fk_calificación` FOREIGN KEY (`id_calificacion`) REFERENCES `calificaciones` (`id_calificacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tutor` FOREIGN KEY (`id_tutor`) REFERENCES `tutores` (`id_tutor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporte_llamadas`
--

LOCK TABLES `reporte_llamadas` WRITE;
/*!40000 ALTER TABLE `reporte_llamadas` DISABLE KEYS */;
INSERT INTO `reporte_llamadas` VALUES (1,1,1,4,'Tematica 1','Recomendacion 1','2017-01-15','2017-01-15',2),(2,2,1,4,'Tematica 1','Recomendacion 1','2017-01-15','2017-01-16',3),(3,3,1,4,'Tematica 1','Recomendacion 1','2017-01-15','2017-01-17',4),(4,4,1,4,'Tematica 1','Recomendacion 1','2017-01-15','2017-01-18',5),(5,5,1,4,'Tematica 1','Recomendacion 1','2017-01-15','2017-01-19',6),(6,1,1,4,'Tematica 2','Recomendacion 2','2017-01-15','2017-01-20',2),(7,2,1,4,'Tematica 2','Recomendacion 2','2017-01-15','2017-01-21',3),(8,3,1,4,'Tematica 2','Recomendacion 2','2017-01-15','2017-01-22',4),(9,4,1,4,'Tematica 2','Recomendacion 2','2017-01-15','2017-01-23',5),(10,5,1,4,'Tematica 2','Recomendacion 2','2017-01-15','2017-01-24',6),(11,1,1,4,'Tematica 3','Recomendacion 3','2017-01-15','2017-01-10',2),(12,2,1,4,'Tematica 3','Recomendacion 3','2017-01-15','2017-01-11',3),(13,3,1,4,'Tematica 3','Recomendacion 3','2017-01-15','2017-01-12',4),(14,4,1,4,'Tematica 3','Recomendacion 3','2017-01-15','2017-01-13',5),(15,5,1,4,'Tematica 3','Recomendacion 3','2017-01-15','2017-01-14',6),(16,1,1,4,'Tematica 4','Recomendacion 4','2017-01-15','2017-01-25',2),(17,2,1,4,'Tematica 4','Recomendacion 4','2017-01-15','2017-01-26',3),(18,3,1,4,'Tematica 4','Recomendacion 4','2017-01-15','2017-01-01',4),(19,4,1,4,'Tematica 4','Recomendacion 4','2017-01-15','2017-01-02',5),(20,5,1,4,'Tematica 4','Recomendacion 4','2017-01-15','2017-01-03',6);
/*!40000 ALTER TABLE `reporte_llamadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_rol` varchar(40) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Tutor');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutores`
--

DROP TABLE IF EXISTS `tutores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutores` (
  `id_tutor` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `id_rol` int(11) NOT NULL,
  `celular` bigint(11) NOT NULL,
  `correo` varchar(150) COLLATE utf8mb4_spanish_ci NOT NULL,
  `contraseña` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  PRIMARY KEY (`id_tutor`),
  UNIQUE KEY `celular_UNIQUE` (`celular`),
  KEY `id_rol1` (`id_rol`),
  CONSTRAINT `fk_roles` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutores`
--

LOCK TABLES `tutores` WRITE;
/*!40000 ALTER TABLE `tutores` DISABLE KEYS */;
INSERT INTO `tutores` VALUES (1,'Cesar Alejandro','Antolinez Poveda',1,3204321811,'cesar@hiline.com','827ccb0eea8a706c4c34a16891f84e7b');
/*!40000 ALTER TABLE `tutores` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-23 10:38:33
