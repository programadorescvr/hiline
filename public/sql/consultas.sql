## Ver clientes

select clientes.id_cliente, clientes.celular,CONCAT(clientes.nombres, ' ', clientes.apellidos) As nombre_cliente, clientes.enfoque, clientes.horario_llamada,
CONCAT(tutores.nombres, ' ', tutores.apellidos),llamadas.fecha, llamadas.llamadas from clientes
inner join tutores on reporte_llamadas.id_tutor = tutores.id_tutor
left join (SELECT id_cliente,max(fecha_llamada) as fecha, count(id_cliente) as llamadas FROM reporte_llamadas group by reporte_llamadas.id_cliente) llamadas on llamadas.id_cliente = clientes.id_cliente;

##ver cliente
Select CONCAT(clientes.nombres, ' ', clientes.apellidos) As nombre_cliente, clientes.celular,
CONCAT(tutores.nombres, ' ', tutores.apellidos) as nombre_tutor,
clientes.horario_llamada,clientes.correo FROM clientes
inner join tutores on tutores.id_tutor = clientes.id_tutor
where clientes.id_cliente = 1;

##Ver registro llamadas cliente
SELECT reporte_llamadas.id_reporte, CONCAT(tutores.nombres, ' ', tutores.apellidos) as nombre_tutor, reporte_llamadas.id_tematica, reporte_llamadas.recomendaciones,
reporte_llamadas.fecha_llamada,reporte_llamadas.duracion, reporte_llamadas.id_calificacion as calificacion
FROM reporte_llamadas
inner join tutores on reporte_llamadas.id_tutor = tutores.id_tutor
where reporte_llamadas.id_cliente = 1;